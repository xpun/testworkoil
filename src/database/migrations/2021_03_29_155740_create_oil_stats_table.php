<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOilStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oil_stats', function (Blueprint $table) {
            $table->id();
            $table->string('company');

            $table->unsignedInteger('fact_qliq_data1');
            $table->unsignedInteger('fact_qliq_data2');
            $table->unsignedInteger('fact_qoil_data1');
            $table->unsignedInteger('fact_qoil_data2');

            $table->unsignedInteger('forecast_qliq_data1');
            $table->unsignedInteger('forecast_qliq_data2');
            $table->unsignedInteger('forecast_qoil_data1');
            $table->unsignedInteger('forecast_qoil_data2');

            $table->date('event_at');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oil_stats');
    }
}
