<!doctype html>
<html lang="ru" style="height: 100%">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#2970FF"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
</head>

<body id="body" style="height: inherit">

<div id="app" style="height: inherit">
    <div v-cloak style="height: inherit">
        <main-app class="v-cloak--hidden"></main-app>
    </div>
</div>

<script src="{{ mix('/js/app.js') }}"></script>

</body>
</html>

