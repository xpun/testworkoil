export const oilRoutes = [
    {
        path: 'oil/stat',
        component: () =>
            import(
                /* webpackChunkName: "IndexOilStat" */ '../../components/oil/IndexOilStat'
                ),
        name: 'IndexOilStat',
    },
    {
        path: 'oil/stat/store',
        component: () =>
            import(
                /* webpackChunkName: "StoreOilStat" */ '../../components/oil/StoreOilStat'
                ),
        name: 'StoreOilStat',
    },
]
