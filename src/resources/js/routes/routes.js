import {oilRoutes} from "./oil/routes";

export const routes = [
    {
        path: '/:lang',
        component: {
            template: '<router-view></router-view>'
        },
        children: [
            ...oilRoutes,
        ]
    }
]
