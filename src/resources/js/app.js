// require('./bootstrap');
import '@babel/polyfill'

import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import '../sass/_variables.scss'

import './plugins/router'
import './plugins/axios'

import MainApp from '../js/components/main/MainApp'

import {i18n} from './plugins/i18n'
import {Trans} from './plugins/translation'

Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)

Vue.use(ElementUI, {
    i18n: (key, value) => i18n.t(key, value)
})

const app = new Vue({
    el: '#app',
    router,
    components: {MainApp},
    i18n,
})
