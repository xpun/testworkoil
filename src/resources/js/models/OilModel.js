export class OilModel {
    static URL = 'oil'

    constructor() {
        this.sum = {
            sum_fact_qliq_: null,
            sum_fact_qoil_: null,
            sum_forecast_qliq_: null,
            sum_forecast_qoil_: null,
        }
    }
}
