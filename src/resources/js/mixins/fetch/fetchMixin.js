export const fetchMixin = {
    data: () => ({
        loading: false
    }),
    methods: {
        fetchData(
            url,
            params = {},
            validateLoading = true,
            toggleLoading = true
        ) {
            return new Promise((resolve, reject) => {
                if (validateLoading && this.loading) {
                    reject(new Error())
                }

                if (toggleLoading) {
                    this.loading = true
                }

                axios
                    .get(url, {
                        params: params
                    })
                    .then((res) => {
                        resolve(res.data)
                    })
                    .catch((err) => {
                        reject(err.response.data.message)
                    })
                    .finally(() => {
                        if (toggleLoading) {
                            this.loading = false
                        }
                    })
            })
        }
    }
}

export const fetchItemsMixin = {
    data: () => ({
        items: {
            data: [],
            current_page: null,
            last_page: null,
        }
    })
}

export const fetchMethodMixin = {
    methods: {
        async fetch(page = 1) {
            this.items.data = []

            let params = this.fetchParams

            params.page = page

            let res = await this.fetchData(this.url, params)

            this.processingFetch(res.data)

            this.setPagination(res)
        }
    },
    computed: {
        url() {
            return null
        },

        fetchParams() {
            return {}
        }
    }
}

export const fetchProcessingMixin = {
    methods: {
        processingFetch(data) {
            data.forEach((item) => {
                item.loading = false

                this.items.data.push(item)
            })
        }
    }
}

export const fetchPaginationMixin = {
    methods: {
        setPagination(res) {
            this.items.current_page = res.current_page

            this.items.last_page = res.last_page
        }
    }
}
