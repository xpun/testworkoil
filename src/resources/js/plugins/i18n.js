import VueI18n from 'vue-i18n'
import Vue from 'vue'
import en from '../lang/en'

export const LANG_DEFAULT = 'en'
export const LANG_FALLBACK = 'en'
export const LANG_SUPPORTED = ['en']

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: LANG_DEFAULT,
  fallbackLocale: LANG_FALLBACK,
  messages: { en },
})
