import axios from 'axios'

import {i18n} from './i18n'

export const I18N_HEADER = 'i18n-lang'

const CSRF = document.head.querySelector('meta[name="csrf-token"]')

window.axios = axios

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

if (CSRF) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = CSRF.content
} else {
    console.error('CSRF token not found')
}

axios.defaults.baseURL = '/api/'

axios.interceptors.request.use(
    (config) => {
        config.headers[I18N_HEADER] = i18n.locale

        return config
    },
    (error) => {
        console.log(error)

        return Promise.reject(error)
    }
)

axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        console.log(error)

        return Promise.reject(error)
    }
)
