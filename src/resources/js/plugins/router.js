import Vue from 'vue'
import VueRouter from 'vue-router'

import {Trans} from './translation'

import {routes} from '../routes/routes'

const CHUNK_ERROR = 'ChunkLoadError'

Vue.use(VueRouter)

global.router = new VueRouter({
    mode: 'history',
    routes: routes,
    scrollBehavior(to, from, position) {
        if (to.matched.some((m) => m.meta.noScroll)) {
            return {x: 0, y: 0}
        }

        return position ? position : {x: 0, y: 0}
    }
})

router.onError((error) => {
    if (error.name === CHUNK_ERROR) {
        window.location.reload()
    }
})

router.beforeEach((to, from, next) => {
    const lang = to.params.lang

    if (!Trans.isLangSupported(lang)) {
        return next(Trans.getUserSupportedLang())
    }

    if (to.name === undefined) {
        next({
            name: 'IndexOilStat',
            params: {lang: lang}
        })
    }

    return Trans.changeLanguage(lang).then(() => next())
})
