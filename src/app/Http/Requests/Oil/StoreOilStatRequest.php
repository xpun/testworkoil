<?php

namespace App\Http\Requests\Oil;

use Illuminate\Foundation\Http\FormRequest;

class StoreOilStatRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'upload'=>'required|file|mimes:xlsx'
        ];
    }
}
