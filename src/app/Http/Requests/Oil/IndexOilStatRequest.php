<?php

namespace App\Http\Requests\Oil;

use App\Services\Oil\OilStatService;
use Illuminate\Foundation\Http\FormRequest;

class IndexOilStatRequest extends FormRequest
{

    public function rules()
    {
        return [
            'date_range' => 'nullable|array|min:2|max:2',
            'date_range.*' => 'required_with:date_range|date_format:' . OilStatService::DATE_FORMAT
        ];
    }
}
