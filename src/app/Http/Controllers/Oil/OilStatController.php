<?php

namespace App\Http\Controllers\Oil;

use App\Http\Controllers\Controller;
use App\Http\Requests\Oil\IndexOilStatRequest;
use App\Http\Requests\Oil\StoreOilStatRequest;
use App\Services\Oil\OilStatService;

class OilStatController extends Controller
{
    public OilStatService $service;

    public function __construct(OilStatService $service)
    {
        $this->service = $service;
    }

    public function index(IndexOilStatRequest $request)
    {
        return $this->service->index($request);
    }

    public function store(StoreOilStatRequest $request)
    {
        $this->service->store($request->file('upload'));
    }
}
