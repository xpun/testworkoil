<?php

namespace App\Imports;

use App\Models\OilStat;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class OilStatImport implements ToCollection, WithChunkReading
{
    public Carbon $createdAt;
    public Carbon $date;

    public array $insert = [];

    public int $insertCount = 0;

    public int $chunkCount = 0;

    const DATE_MONTH = 3;
    const DATE_YEAR = 2021;

    const CHUNK_STEP = 1000;

    const START_DATA_INDEX = 3;

    const OFFSET_ID = 0;
    const OFFSET_COMPANY = 1;
    const OFFSET_FACT_QLIQ_DATA1 = 2;
    const OFFSET_FACT_QLIQ_DATA2 = 3;
    const OFFSET_FACT_QOIL_DATA1 = 4;
    const OFFSET_FACT_QOIL_DATA2 = 5;
    const OFFSET_FORECAST_QLIQ_DATA1 = 6;
    const OFFSET_FORECAST_QLIQ_DATA2 = 7;
    const OFFSET_FORECAST_QOIL_DATA1 = 8;
    const OFFSET_FORECAST_QOIL_DATA2 = 9;

    public function __construct()
    {
        $this->createdAt = now();

        $this->date = now()
            ->setYear(self::DATE_YEAR)
            ->setMonth(self::DATE_MONTH);
    }

    public function chunkSize(): int
    {
        return self::CHUNK_STEP;
    }

    public function insertData()
    {
        if (!$this->insertCount) return;

        OilStat::insert($this->insert);

        $this->insert = [];

        $this->insertCount = 0;
    }

    public function collection(Collection $rows)
    {
        $this->chunkCount += 1;

        foreach ($rows as $key => $value) {
            $index = $key * $this->chunkCount;

            if ($index < self::START_DATA_INDEX) continue;

            $this->insertCount += 1;

            $this->insert[] = [
                'id' => $value[self::OFFSET_ID],
                'company' => $value[self::OFFSET_COMPANY],
                'fact_qliq_data1' => $value[self::OFFSET_FACT_QLIQ_DATA1],
                'fact_qliq_data2' => $value[self::OFFSET_FACT_QLIQ_DATA2],
                'fact_qoil_data1' => $value[self::OFFSET_FACT_QOIL_DATA1],
                'fact_qoil_data2' => $value[self::OFFSET_FACT_QOIL_DATA2],
                'forecast_qliq_data1' => $value[self::OFFSET_FORECAST_QLIQ_DATA1],
                'forecast_qliq_data2' => $value[self::OFFSET_FORECAST_QLIQ_DATA2],
                'forecast_qoil_data1' => $value[self::OFFSET_FORECAST_QOIL_DATA1],
                'forecast_qoil_data2' => $value[self::OFFSET_FORECAST_QOIL_DATA2],
                'event_at' => $this->date->copy()->setDay(random_int(1, 30))->toDate(),
                'created_at' => $this->createdAt,
                'updated_at' => $this->createdAt,
            ];

            if ($this->insertCount > self::CHUNK_STEP) {
                $this->insertData();
            }
        }

        $this->insertData();
    }
}
