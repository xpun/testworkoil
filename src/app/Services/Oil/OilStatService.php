<?php


namespace App\Services\Oil;


use App\Http\Requests\Oil\IndexOilStatRequest;
use App\Imports\OilStatImport;
use App\Models\OilStat;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class OilStatService
{
    const DATE_FORMAT = 'd/m/Y';

    const PAGINATION = 20;

    const KEYS = ['data1', 'data2'];

    public function index(IndexOilStatRequest $request): array
    {
        $query = OilStat::query();

        if ($request->date_range) {
            $rangeStart = Carbon::createFromFormat(
                self::DATE_FORMAT,
                $request->date_range[0]
            )->toDateString();

            $rangeEnd = Carbon::createFromFormat(
                self::DATE_FORMAT,
                $request->date_range[1]
            )->toDateString();

            $query->whereBetween('event_at', [$rangeStart, $rangeEnd]);
        }

        foreach (self::KEYS as $KEY) {
            $query->selectRaw("
                SUM(fact_qliq_$KEY) sum_fact_qliq_$KEY,
                SUM(fact_qoil_$KEY) sum_fact_qoil_$KEY,
                SUM(forecast_qliq_$KEY) sum_forecast_qliq_$KEY,
                SUM(forecast_qoil_$KEY) sum_forecast_qoil_$KEY
                ");
        }

        return [
            'sum' => $query->get()->first(),
            'keys' => self::KEYS
        ];
    }

    public function store(UploadedFile $file)
    {
        DB::transaction(function () use ($file) {
            OilStat::query()->delete();

            Excel::import(new OilStatImport(), $file);
        });
    }
}
