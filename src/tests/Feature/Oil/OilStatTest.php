<?php

namespace Tests\Feature\Oil;

use App\Models\OilStat;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class OilStatTest extends TestCase
{
    const URL = 'api/oil';
    const FILE_NAME = 'test-upload.xlsx';
    const FILE_MIME_TYPE = '.xlsx';
    const FILE_STAT_COUNT = 20;

    public function testIndex()
    {
        $this
            ->json('GET', self::URL)
            ->assertSuccessful()
            ->assertJsonStructure(['sum', 'keys']);
    }

    public function testStore()
    {
        $path = public_path(self::FILE_NAME);

        $upload = new UploadedFile(
            $path,
            self::FILE_NAME,
            self::FILE_MIME_TYPE,
            null,
            true
        );

        $this
            ->json('post', self::URL, ['upload' => $upload])
            ->assertSuccessful();

        $this->assertTrue(OilStat::query()->count() === self::FILE_STAT_COUNT);
    }
}
